<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Product Management</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: purple">
			<div>
				<a class="navbar-brand"><b style="color: white">ShopKart</b></a>
			</div>
			<ul class="navbar-nav">
				<li>
				<a href="<%=request.getContextPath()%>/cat" class="nav-link">Shopping</a>
				<li>
				<li>
				<a href="<%=request.getContextPath()%>/orderpage" class="nav-link">My Orders</a>
				<li>
			</ul>
			<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
		  		<ul class="navbar-nav ">
					<li class="nav-item">
			  			<a href="<%=request.getContextPath()%>/loginPage" class="btn btn-outline-warning my-2 my-sm-0">Log out</a>
					</li>	
		  		</ul>		  
			</div>
		</nav>
	</header>
	<br>
	<div class="row">
		<div class="container">
			<h3 class="text-center">Your orders</h3>
			<hr>
			<table class="table table-bordered">
				<thead style="background-color:purple;color: white">
					<tr>
						<td>Product Name</td>
						<td>Price</td>
						<td>Quantity</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="order" items = "${orderDetails}">
						<tr>
							<td><c:out value = "${order.prod_name}"/></td>
							<td><c:out value = "${order.prod_price}"/></td>
							<td><c:out value = "${order.prod_qty}"/></td>
							<td><a href="deleteOrder?prod_name=<c:out value='${order.prod_name}'/>" class="btn btn-danger">Cancel</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>