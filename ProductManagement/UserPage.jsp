<%@page import="java.io.PrintWriter"%>
<%@page import="com.prasad.productmanagement.bean.Product"%>
<%@page import="com.prasad.productmanagement.dao.CategoryDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.Map" %>
<html>
<head>
<title>Product Management</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: purple">
			<div>
				<a class="navbar-brand"><b style="color: white">ShopKart</b></a>
			</div>
			<ul class="navbar-nav">
				<li>
				<a href="<%=request.getContextPath()%>/cat" class="nav-link">Shopping</a>
				<li>
				<li>
				<a href="<%=request.getContextPath()%>/orderpage" class="nav-link">My Orders</a>
				<li>
				<c:if test="${uId == 'site_admin' }">
				<li>
				<a href="<%=request.getContextPath()%>/list" class="nav-link">Product Details</a>
				<li>
				</c:if>
			</ul>
			<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
		  		<ul class="navbar-nav ">
					<li class="nav-item">
			  			<a href="<%=request.getContextPath()%>/loginPage" class="btn btn-outline-warning my-2 my-sm-0">Log out</a>
					</li>	
		  		</ul>		  
			</div>
		</nav>
	</header>
	<br>
	<div class="row">
		<div class="container">
			<h3 class="text-center">ShopKart</h3>
			<hr>
			<div class="container text-center">
			
			<c:if test="${prodCat != null}">
			<form class="form-group" id="searchForm"  onsubmit="onSubmit()" >
				<select class="form-control" name="category" id="category">
  					<option selected>Choose product category</option>
  					<c:forEach var="cat" items = "${prodCat}">
  						<option ><c:out value = "${cat.category}"/></option>
  						<!-- <a href="shop?category=<c:out value='${cat.category}'/>" class="btn btn-primary" >search</a>  -->
  					</c:forEach>
				</select>
				<button type="submit" class="btn btn-success m-2">search</button>
			</form>
			</c:if>
			
			<c:if test="${products != null }">
			<h5>To choose another category click on Shopping </h5>
			</c:if>
			</div>

			<hr>
			<table class="table table-borderless">
				<thead style="background-color:purple;color: white">
					<tr>
						<td>Category</td>
						<td>Name</td>
						<td>Description</td>
						<td>Price</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>
				
					<c:forEach var="product" items = "${products}">
						<tr>
							<td><img src="https://smath12.s3.ap-south-1.amazonaws.com/<%=request.getParameter("category")%>"  style=" width: 65px; height: 65px; object-fit: cover;"/> </td>
							<td><c:out value = "${product.name}"/></td>
							<td><c:out value = "${product.desc}"/></td>
							<td><c:out value = "${product.price}"/></td>
							<td><a href="mycart?name=<c:out value='${product.name}'/>&price=<c:out value='${product.price}'/>" class="btn btn-primary"> Add</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
		</div>
	</div>
	<script type="text/javascript">
			function onSubmit(){
				 var action_src = "http://localhost:8080/ProductManagement/shop?category=" + document.getElementById("category")[0].value;
				    var your_form = document.getElementById('searchForm');
				    your_form.action = action_src ;
			}	
	</script>
</body>
</html>