package com.prasad.productmanagement.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.Part;


public class CategoryDao {
	
	private static String driverClass = "org.mariadb.jdbc.Driver";
	private static String url = "jdbc:mariadb://localhost:3306/productmanagement";
	private static String user = "root";
	private static String password = "H@sourabh123";
	
	private static final String INSERT_CAT_SQL = "insert into categorydetails values (?);";
	private static final String SELECT_CAT_SQL = "select category_name from categorydetails";
	private static final String SELECT_CAT_IMG_SQL = "select category_name,category_image from categorydetails";
	
	protected Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName(driverClass);
			conn = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return conn;
	}

	
	public void addc_name(String cat_name)
	{
		try(Connection conn = getConnection();
				PreparedStatement prepstat = conn.prepareStatement(INSERT_CAT_SQL)){
			prepstat.setString(1, cat_name);
			prepstat.execute();
			 //System.out.println(prodDetails);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	
	
	
	
	public void addCategory(String cat_name,Part cat_img) throws Exception 
	{
		 //System.out.println(INSERT_PRODUCT_SQL);
		
		
	       String fileName =getFileName(cat_img);	
		
		System.out.println("finmenen   "+fileName );
		s3util.upload(cat_name,cat_img.getInputStream());
		
	 }
	
	
	private String getFileName(Part cat_img)
	{
		String contentDisposition = cat_img.getHeader("content-disposition");
		int beginIndex = contentDisposition.indexOf("filename=")+10;
		int endIndex = contentDisposition.length()-5;
		return contentDisposition.substring(beginIndex,endIndex);
	}
	
	
	
	
	
	
	
	
	
	
	
	public List<String> selectCatFromCat() {
		List<String> catList = new ArrayList<>();
		try(Connection conn = getConnection();
				PreparedStatement prepstat = conn.prepareStatement(SELECT_CAT_SQL)){
			 ResultSet rs = prepstat.executeQuery();
			 
			 while(rs.next()) {
				 String cat = rs.getString("category_name");
				 catList.add(cat);
			 }
			 //System.out.println(prodDetails);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return catList;
	}
	


}
