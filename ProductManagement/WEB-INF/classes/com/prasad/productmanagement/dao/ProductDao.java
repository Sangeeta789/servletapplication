package com.prasad.productmanagement.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.prasad.productmanagement.bean.Product;

public class ProductDao {
	
	private static String driverClass = "org.mariadb.jdbc.Driver";
	private static String url = "jdbc:mariadb://localhost:3306/productmanagement";
	private static String user = "root";
	private static String password = "H@sourabh123";
	
	private static final String INSERT_PRODUCT_SQL = "insert into productdetails values (?,?,?,?,?,?);";
	private static final String SELECT_PRODUCT_BY_ID = "select * from productdetails where id = ?;";
	private static final String SELECT_ALL_PRODUCTS = "select * from productdetails";
	private static final String DELETE_PRODUCT_BY_ID = "delete from productdetails where id = ?;";
	private static final String UPDATE_PRODUCT_BY_ID = "update productdetails set name = ?, `desc` = ?, price = ?, qty = ?, category = ? where id = ?;";
	private static final String SELECT_PRODUCT_BY_CATEGORY = "select * from productdetails where category = ?;";
	private static final String SELECT_DISTINCT_CATEGORY = "select distinct category from productdetails";
	
	public ProductDao() {
		
	}
	
	Gson gson = new Gson();
	
	protected Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName(driverClass);
			conn = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	 public void addProduct(String prodJSON) {
		 //System.out.println(INSERT_PRODUCT_SQL);
		 try(Connection conn = getConnection();
				 PreparedStatement prepstat = conn.prepareStatement(INSERT_PRODUCT_SQL)){
			 Product prod = gson.fromJson(prodJSON, Product.class);
			 //System.out.println(prod);
			 prepstat.setInt(1, prod.getId());
			 prepstat.setString(2,prod.getName());
			 prepstat.setString(3,prod.getDesc());
			 prepstat.setDouble(4, prod.getPrice());
			 prepstat.setInt(5, prod.getQty());
			 prepstat.setString(6, prod.getCategory());
			 //System.out.println(prepstat);
			 prepstat.executeUpdate();
		 }catch(SQLException e) {
			 e.printStackTrace();
		 }
	 }
	 
	public String showProductById(int id) {
		Product prod = null;
		String prodJSON;
		try(Connection conn = getConnection();
				 PreparedStatement prepstat = conn.prepareStatement(SELECT_PRODUCT_BY_ID)){
			 prepstat.setInt(1, id);
			 //System.out.println(prepstat);
			 ResultSet rs = prepstat.executeQuery();
			 
			 while(rs.next()) {
				 int pid = rs.getInt("id");
				 String name = rs.getString("name");
				 String desc = rs.getString("desc");
				 double price = rs.getDouble("price");
				 int qty = rs.getInt("qty");
				 String category = rs.getString("category");
				 prod = new Product(pid,name,desc,price,qty,category);
			 }
		}catch(SQLException e) {
			e.printStackTrace();
		}
		prodJSON = gson.toJson(prod);
		return prodJSON;
	
	}
	
	public String showProducts(){
		List<Product> prodDetails = new ArrayList<>();
		String prodDetailsJSON;
		try(Connection conn = getConnection();
				 PreparedStatement prepstat = conn.prepareStatement(SELECT_ALL_PRODUCTS)){
			 //System.out.println(prepstat);
			 ResultSet rs = prepstat.executeQuery();
			 
			 while(rs.next()) {
				 int id = rs.getInt("id");
				 String name = rs.getString("name");
				 String desc = rs.getString("desc");
				 double price = rs.getDouble("price");
				 int qty = rs.getInt("qty");
				 String category = rs.getString("category");
				 Product prod = new Product(id,name,desc,price,qty,category);
				 prodDetails.add(prod);
			 }
			 //System.out.println(prodDetails);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		prodDetailsJSON = gson.toJson(prodDetails); 
		return prodDetailsJSON;
	}
	
	public boolean editProduct(String prodJSON) throws SQLException {
		boolean productUpdated;
		 try(Connection conn = getConnection();
				 PreparedStatement prepstat = conn.prepareStatement(UPDATE_PRODUCT_BY_ID)){
			 Product prod = gson.fromJson(prodJSON, Product.class);
			 prepstat.setString(1,prod.getName());
			 prepstat.setString(2,prod.getDesc());
			 prepstat.setDouble(3, prod.getPrice());
			 prepstat.setInt(4, prod.getQty());
			 prepstat.setString(5, prod.getCategory());
			 prepstat.setInt(6, prod.getId());
			 //System.out.println("Updated product: "+prepstat);
			 productUpdated = prepstat.executeUpdate() > 0;
			 
		 }
		return productUpdated;
	}
	
	public boolean deleteProduct(int id) throws SQLException {
		boolean prodDeleted;
		try(Connection conn = getConnection();
				 PreparedStatement prepstat = conn.prepareStatement(DELETE_PRODUCT_BY_ID)){
			prepstat.setInt(1, id);
			prodDeleted = prepstat.executeUpdate() > 0;
		}
		return prodDeleted;
	}
	
	public String showProductByCategory(String category) {
		List<Product> prodDetails = new ArrayList<>();
		Product prod = null;
		String prodJSON;
		try(Connection conn = getConnection();
				 PreparedStatement prepstat = conn.prepareStatement(SELECT_PRODUCT_BY_CATEGORY)){
			 prepstat.setString(1, category);
			 //System.out.println(prepstat);
			 ResultSet rs = prepstat.executeQuery();
			 
			 while(rs.next()) {
				 int pid = rs.getInt("id");
				 String name = rs.getString("name");
				 String desc = rs.getString("desc");
				 double price = rs.getDouble("price");
				 int qty = rs.getInt("qty"); 
				 prod = new Product(pid,name,desc,price,qty,category);
				 prodDetails.add(prod);
				// System.out.println(prod);
			 }
		}catch(SQLException e) {
			e.printStackTrace();
		}
		prodJSON = gson.toJson(prodDetails);
		return prodJSON;
	
	}
	
	public String showCategory() {
		List<Product> prodDetails = new ArrayList<>();
		String prodDetailsJSON;
		try(Connection conn = getConnection();
				 PreparedStatement prepstat = conn.prepareStatement(SELECT_DISTINCT_CATEGORY)){
			 //System.out.println(prepstat);
			 ResultSet rs = prepstat.executeQuery();
			 
			 while(rs.next()) {
				 String category = rs.getString("category");
				 Product prod = new Product(category);
				 prodDetails.add(prod);
			 }
			 //System.out.println(prodDetails);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		prodDetailsJSON = gson.toJson(prodDetails); 
		return prodDetailsJSON;
	}
	
}
