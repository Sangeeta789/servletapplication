package com.prasad.productmanagement.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.prasad.productmanagement.bean.MyOrders;
import com.prasad.productmanagement.bean.UserData;


public class UserDao {
	
	String userDao1;
	
	private static String driverClass = "org.mariadb.jdbc.Driver";
	private static String url = "jdbc:mariadb://localhost:3306/productmanagement";
	private static String user = "root";
	private static String password = "H@sourabh123";
	
	private static final String INSERT_USER_SQL = "insert into `user` values (?,?,?,?,?,?);";
	private static final String INSERT_ORDER_SQL = "insert into orderdetails values (?,?,?,?)";
	private static final String SELECT_ORDER_SQL = "select prod_name,prod_price,prod_qty from orderdetails where user_id=? ";
	private static final String DELETE_ORDER_SQL = "delete from orderdetails where prod_name=?";
	
	private static final String SELECT_USER_SQL_BY_PWD = "select userId from `user` where userPassword = ?";
	
	
	public UserDao() {
		
	}
	
	public UserDao(String userJSON) {
		this.userDao1 = userJSON;
	}
	
	Gson gson = new Gson();
	
	protected Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName(driverClass);
			conn = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public void addUser() {
		 //System.out.println(INSERT_PRODUCT_SQL);
		 try(Connection conn = getConnection();
				 PreparedStatement prepstat = conn.prepareStatement(INSERT_USER_SQL)){
			 UserData user = gson.fromJson(userDao1, UserData.class);
			 //System.out.println(user);
			 prepstat.setString(1, user.getUser_fname());
			 prepstat.setString(2,user.getUser_lname());
			 prepstat.setString(3,user.getUser_id());
			 prepstat.setString(4, user.getUser_add());
			 prepstat.setLong(5, user.getUser_phone());
			 prepstat.setString(6, user.getUser_password());
			//System.out.println(prepstat);
			 prepstat.executeUpdate();
		 }catch(SQLException e) {
			 e.printStackTrace();
		 }
	 }
	
public boolean validateLogin(String uId,String pwd) {
		boolean status = false;
		
	
		 try(Connection conn = getConnection();
				 PreparedStatement prepstat = conn.prepareStatement(SELECT_USER_SQL_BY_PWD )){
			 prepstat.setString(1, pwd);
			 ResultSet rs = prepstat.executeQuery();
			 while(rs.next()) {
				  String userIdval = rs.getString("userId");

					 if(uId.contentEquals(userIdval)) {
						 status = true;
					 }
				 
			 }
			 }catch(SQLException e) {
				 e.printStackTrace();
				 }
		 
			 
		return status;
	}
	
	public void addOrderDeatails(String uid,String name,double price,int qty) {
		 try(Connection conn = getConnection();
				 PreparedStatement prepstat = conn.prepareStatement(INSERT_ORDER_SQL)){
			 prepstat.setString(1, uid);
			 prepstat.setString(2,name);
			 prepstat.setDouble(3,price);
			 prepstat.setInt(4, qty);
			//System.out.println(prepstat);
			 prepstat.executeUpdate();
		 }catch(SQLException e) {
			 e.printStackTrace();
		 }
	}
	
	public String orderDetailsList(){
		List<MyOrders> orderDetails = new ArrayList<>();
		String orderJSON;
		try(Connection conn = getConnection();
				 PreparedStatement prepstat = conn.prepareStatement(SELECT_ORDER_SQL)){
			prepstat.setString(1, UserData.getUser_id());
			ResultSet rs = prepstat.executeQuery();
			 while(rs.next()) {
				 String prodName = rs.getString("prod_name");
				 double prodPrice = rs.getDouble("prod_price");
				 int prodQty = rs.getInt("prod_qty");
				 MyOrders newOrder = new MyOrders(prodName,prodPrice,prodQty);
				 orderDetails.add(newOrder);
			 }
		}catch(SQLException e) {
			 e.printStackTrace();
		 }
		orderJSON = gson.toJson(orderDetails);
		return orderJSON;
	}
	
	public boolean deleteOrder(String prod_name) throws SQLException {
		boolean orderDeleted;
		try(Connection conn = getConnection();
				 PreparedStatement prepstat = conn.prepareStatement(DELETE_ORDER_SQL)){
			prepstat.setString(1, prod_name);
			orderDeleted = prepstat.executeUpdate() > 0;
		}
		return orderDeleted;
	}
	
	
}
