package com.prasad.productmanagement.bean;

public class UserData {
   static String user_id;
	String user_fname;
	String user_lname;
	String user_add;
	long user_phone;
	String user_password;
	
	
	public UserData(String user_id, String user_fname, String user_lname, String user_add, long user_phone,String user_password) {
		super();
		this.user_fname = user_fname;
		this.user_lname = user_lname;
		this.user_id = user_id;
		this.user_add = user_add;
		this.user_phone = user_phone;
		this.user_password = user_password;
	}
	
	public UserData(String u_id) {
		this.user_id = u_id;
	}
	
	public UserData() {
		super();
	}
	
	public static String getUser_id() {
		return user_id;
	}
	public static void setUser_id(String user_id1) {
		user_id = user_id1;
	}
	public String getUser_fname() {
		return user_fname;
	}
	public void setUser_fname(String user_fname) {
		this.user_fname = user_fname;
	}
	public String getUser_lname() {
		return user_lname;
	}
	public void setUser_lname(String user_lname) {
		this.user_lname = user_lname;
	}
	public String getUser_add() {
		return user_add;
	}
	public void setUser_add(String user_add) {
		this.user_add = user_add;
	}
	public long getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(long user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	@Override
	public String toString() {
		return "UserData [user_id=" + user_id + ", user_fname=" + user_fname + ", user_lname=" + user_lname
				+ ", user_add=" + user_add + ", user_phone=" + user_phone + ", user_password=" + user_password + "]";
	}
	
	
	
}
