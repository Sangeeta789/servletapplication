package com.prasad.productmanagement.bean;

public class MyOrders {
	String user_id;
	String prod_name;
	double prod_price;
	int prod_qty;
	
	public MyOrders() {}
	
	
	
	public MyOrders(String prod_name, double prod_price, int prod_qty) {
		super();
		this.prod_name = prod_name;
		this.prod_price = prod_price;
		this.prod_qty = prod_qty;
	}
	public MyOrders(String user_id, String prod_name, double prod_price, int prod_qty) {
		super();
		this.user_id = user_id;
		this.prod_name = prod_name;
		this.prod_price = prod_price;
		this.prod_qty = prod_qty;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getProd_name() {
		return prod_name;
	}
	public void setProd_name(String prod_name) {
		this.prod_name = prod_name;
	}
	public double getProd_price() {
		return prod_price;
	}
	public void setProd_price(double prod_price) {
		this.prod_price = prod_price;
	}
	public int getProd_qty() {
		return prod_qty;
	}
	public void setProd_qty(int prod_qty) {
		this.prod_qty = prod_qty;
	}
	
	
	
}
