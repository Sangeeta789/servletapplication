package com.prasad.productmanagement.bean;

public class Product {
	
	private int id;
	private String name;
	private String desc;
	private double price;
	private int qty;
	private String category;
	
	public Product() {}
	
	public Product(String name, String desc, double price, int qty, String category) {
		super();
		this.name = name;
		this.desc = desc;
		this.price = price;
		this.qty = qty;
		this.category = category;
	}
	
	
	public Product(int id, String name, String desc, double price, int qty, String category) {
		super();
		this.id = id;
		this.name = name;
		this.desc = desc;
		this.price = price;
		this.qty = qty;
		this.category = category;
	}
	
	public Product(int id, String name, String desc, double price, int qty) {
		super();
		this.id = id;
		this.name = name;
		this.desc = desc;
		this.price = price;
		this.qty = qty;
	}
	
	public Product(String category) {
		super();
		this.category = category;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", desc=" + desc + ", price=" + price + ", qty=" + qty + ", category=" + category + "]";
	}
	
	
	
}
