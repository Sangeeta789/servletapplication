<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<style>
		body{
			background-image: url('https://t3.ftcdn.net/jpg/00/21/70/82/240_F_21708280_RFKz4O7ImQluB9FgX2mUYFUNDmlLokX6.jpg');
			background-size: cover;
			background-position: center;
			color: #ffffffd6;
		}
	</style>
<title>ShopKart</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"    ;
			>
			<div>
				<a class="navbar-brand"><b style="color: white;font-size: xx-large;">PRODUCT MANAGEMENT</b></a>
			</div>
			<ul class="navbar-nav" >
				<li>
				<a href="<%=request.getContextPath()%>/showRegister" class="nav-link" style="font-size: x-large">Sign up</a>
				<li>
				<li>
				<a href="<%=request.getContextPath()%>/loginPage" class="nav-link" style="font-size: x-large">Log in</a>
				<li>
			</ul>
		</nav>
	</header>
	<br>
	<div class="row">
		<div class="container">
			<h3 class="text-center" style ="color: #f6f9fc">Sign Up</h3>
			<hr>
			<div class="container" style="width:500px">
				<form class="form-group" action="register" method="POST">
					<label style ="color: #f6f9fc">User ID</label> <input type="text" class="form-control m-2 "
						name="uId" />
					<label style ="color: #f6f9fc">First Name</label> <input type="text" class="form-control m-2 "
						name="fname"/>
					<label style ="color: #f6f9fc">Last Name</label> <input type="text" class="form-control m-2 "
						name="lname"/>
					<label style ="color: #f6f9fc">Address</label> <input type="text" class="form-control m-2 "
						name="add"/>
					<label style ="color: #f6f9fc">Phone</label> <input type="number" class="form-control m-2 "
						name="phone"/>
					<label style ="color: #f6f9fc">Password</label> <input type="password" class="form-control m-2 "
						name="pwd"/>
					
					<input class="btn btn-success m-2" type="submit" name="submit" value="Register"/>
				</form>
			</div>
		</div>
	</div>
</body>
</html>